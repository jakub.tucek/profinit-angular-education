# Tasks

[[_TOC_]]

## Requirements

1. IDE (WebStorm, IntelliJ, VSCode, ...)
    - WebStorm/IntelliJ EAP or trial
    - Free VS Code (https://code.visualstudio.com/), and install Angular Language Service extension
2. Some browser (IE is not a browser)
3. GIT
4. NodeJS (https://nodejs.org/en/) - latest LTS
5. (OPTIONAL) Angular CLI (`npm i -g @angular/cli`)
6. (OPTIONAL) Yarn (`npm i -g yarn`) - better/faster alternative to npm

## Setup

1. Clone repository

   ```shell
   git clone https://gitlab.com/jakub.tucek/profinit-angular-education.git

   or

   git clone git@gitlab.com:jakub.tucek/profinit-angular-education.git
   ```

2. Open _client_ folder in IDE
3. Run

   ```shell
   cd client

   npm install

   npm run start (or yarn start)
   ```

4. Open browser at http://localhost:4200

## Task 1 - Create new component

Create a new page that will display a list of beers:

1. Create a new component named `BeersComponent` in _src/app/modules/core/pages_ that will represent the new page.
    - Only basic (skeleton) version of the component is required. The page should only contain text "Beers".
    - 💡 Intro to components (only the part before "Component
      metadata") - https://angular.io/guide/architecture-components
2. Register it in _client/src/app/modules/core/core.module.ts_ (in `declarations`).
    - 💡 Intro to modules (esp. "NgModule metadata") - https://angular.io/guide/architecture-modules
3. Register a new route for the created component in _core-routing.module.ts_.
    - Name the route according to the corresponding `routerLink` defined in _header.component.html_.
    - 💡 Defining a basic route - https://angular.io/guide/router#defining-a-basic-route

## Task 2 - Prepare data definition

1. Create a TypeScript interface for beer and add it as a type hint in _beers-mock.service.ts_ and
   _beers-service.interface.ts_.
    - Replace `any` with your new type in _beer-mock.service.ts_.
      Structure should match the data defined in the mock service (see `beers`).

## Task 3 - Display data

1. Register the beers mock service to Angular context by adding it to `providers` in _core.module.ts_:
   ```typescript
   { provide: 'BeersService', useClass: BeersMockService }
   ```
    - 💡 Configuring dependency providers (especially "Class providers:
      useClass") - https://angular.io/guide/dependency-injection-providers
2. Inject the beers service interface into your component using the token (IOC - top down):
   ```typescript
   constructor(@Inject('BeersService') private beersService: BeersService) {}
   ```
3. Using the `OnInit` lifecycle hook in `BeersComponent`, load data by calling the synchronous method `getBeers`
   from the beers service.
    - 💡 Lifecycle hooks (esp. "Responding to lifecycle events") - https://angular.io/guide/lifecycle-hooks
4. Create new components `BeersTableComponent` and `BeersRowComponent`, and display the data obtained in `ngOnInit`
   using these components.
    - Hierarchy should be the following: `BeersComponent -> BeersTableComponent -> BeersRowComponent`.
    - Pass data from `BeersComponent` to the child components using data binding (
      see https://angular.io/guide/inputs-outputs).
    - Display data in `BeersTableComponent` using `*ngFor` in its template (
      see https://angular.io/guide/structural-directives, esp. "Structural directive shorthand").
    - ❗ If you want to use `table`, `tr` and `td` HTML elements, take a look this page on how to properly
      define `BeersRowComponent` - https://stackoverflow.com/questions/62653507/angular-child-component-breaks-table.

## Task 4 - Display data asynchronously

1. Load data in `ngOnInit` inside `BeersComponent` using the asynchronous method `getBeersAsync` from the beers
   service.
    - ❗ Use `subscribe` to get data from the returned `Observable` (see https://rxjs.dev/guide/subscription).

## Task 5 - Pipes

1. Use an existing pipe or create a new one that will transform `createdAt` property of type `Date` to
   the `DD.MM.YYYY HH:mm` format.
    - 💡 Transforming data using pipes - https://angular.io/guide/pipes (until "Detecting changes with data binding in
      pipes")
2. Create another pipe that will append the degree symbol to input.
    - For example, `{{ beer.degreeOfBeer | appendDegree }}`, where `degreeOfBeer` is `12`, should produce `12°`.
    - ❗ Don't forget to register the pipe in _core.module.ts_.
3. Write unit tests for the `appendDegree` pipe.
    - 💡 Unit testing pipes in Angular - https://www.amadousall.com/angular-pipes-unit-testing-angular-pipes/

## Task 6 - Directives

1. Create a directive that makes the row/column a different color if the beer is from Prague.
    - 💡 Attribute directives - https://angular.io/guide/attribute-directives
    - ❗ Don't forget to register the directive in _core.module.ts_.

## Task 7 - Reactive forms

Create a new page that will add new beers to the collection.

1. Extend the beers service with a new method named `createBeer`:
   ```typescript
   createBeer(newBeer: BeerFormValues): Observable<Beer>;
   ```
2. The new method should create a new beer from the given form input, add it to the collection of all beers, and return
   the created beer.
   ```typescript
   return of(createdBeer);
   ```
3. Create a page containing a reactive form for creating beers (create a new route and one or more components for the
   new page).
    - 💡 Reactive forms - https://angular.io/guide/reactive-forms
    - 💡 Typed reactive forms - https://angular.io/guide/typed-forms
    - ❗ Don't forget to import `ReactiveFormsModule` in _core.module.ts_.
4. Form controls should have at least the "required" validation. It should not be possible to submit an invalid form.
    - You can use `(ngSubmit)="someFunction()"` to submit form data, see documentation.
    - ❗ When submitting the form, you can use either `this.formGroup.value` or `this.formGroup.getRawValue()` to get
      current values. Explore both options and use the correct one.
5. After submitting the form, you can optionally add a redirect to the beers page by using `Router`.

   ```typescript
   constructor(private router: Router) { /* ... */ }

   handleSubmitForm() {
     // ...
     this.router.navigate(["/beers"]);
   }
   ```

## Task 8 - Template-driven forms (OPTIONAL)

**Warning: Commit your changes from the Task 7 first, we will use them later.**

1. Recreate the same functionality as in the previous task, but using template-driven forms.
    - 💡 Template-driven forms - https://angular.io/guide/forms
    - ❗ Don't forget to import `FormsModule` in _core.module.ts_.

## Task 9 - Use external API

In this task, we will fetch data from the external API with the endpoints documented in the following link:

https://profinit-angular-education.fly.dev/api-docs/#/

1. Create a new service class implementing `BeersService`. Use it as the implementation instead of `BeersMockService`
   in `CoreModule`.
2. Inject `HttpClient` into the created service (you will need to import `HttpClientModule`).
3. Implement the needed methods to call the API (according to the `BeersService` interface).
    - You can find out the type of returned data by adding debug breakpoints or by manually exploring the endpoints in
      browser.
    - 💡 Communicating with backend services using HTTP (esp. "Requesting data from a server" and "Sending data to a
      server") - https://angular.io/guide/http
4. Use the new service in your components.
    - Think whether you really need to change anything in the components. Is it necessary to do something other than
      changing the implementation of `BeersService` in `CoreModule` if we inject it as show below?
      ```typescript
      @Inject("BeersService") private beersService: BeersService
      ```

## Task 10 - Delete

1. Extend the `BeersService` interface with a method for deleting beers.
2. Add delete buttons to the table rows that will call the created delete method.
    - You can use `(click)` to process click events on elements.
    - 💡 Event binding - https://angular.io/guide/event-binding
3. Re-fetch data after deleting an item.
    - This task can be done in a variety of ways. For example, you can practice your skills in using RxJS (
      e.g., `BehaviorSubject`, `Subject`, operators such as `switchMap`).

## Task 11 - Tests

1. Write tests for displaying data using own mock service.

    - Mocking service in test context can be done like this:
      ```typescript
      providers: [
        { provide: "BeersService", useClass: TestBeersMockService },
      ];
      ```
    - `TestBeersMockService` is your new class implementing `BeersService` interface.

2. See _home.component.spec.ts_ test for inspiration.
    - 💡 Intro to testing - https://angular.io/guide/testing
    - 💡 Basics of testing components - https://angular.io/guide/testing-components-basics

## Task 12 - Update

1. Extend the `BeersService` interface with a method for updating a beer (which accepts ID and form data) and for
   getting
   a beer by ID.
    - Note that the API doesn't have a method to get a beer by ID. You can use `getBeersAsync` and then map the result
      to a single value using the RxJS `map` operator.
    - 💡 RxJS `map` - https://rxjs.dev/api/operators/map
2. Create a new component (page) for beer update with a URL parameter for the beer ID (e.g., `beers/update/:id`).
    - 💡 Parametrised routes - https://codecraft.tv/courses/angular/routing/parameterised-routes/
3. When entering the created page, load the beer by ID and update the form with the beer data.
    - 💡 How to set form data from an object - https://angular.io/guide/reactive-forms#updating-parts-of-the-data-model
    - ❗ In the above link, note the difference between `setValue` and `patchValue`.
4. Add logic to handle beer update.

## Task 13 - Validators

1. Add a form validator that will check that the name is at least 5 characters long.
    - 💡 Built-in validators - https://angular.io/api/forms/Validators#minlength
2. Create a custom validator that will check that the city of origin starts with a capital letter.
    - 💡 Defining custom validators - https://angular.io/guide/form-validation#defining-custom-validators
3. Display error messages below form fields when there are errors.

## Task 14 - Async validators

1. Create a custom async validator that will check if the name of a beer is already used.
    - 💡 Implementing a custom async
      validator - https://angular.io/guide/form-validation#implementing-a-custom-async-validator
    - Since the created validator has to call the API to check if the entered name is already in use, we want to prevent
      checks after each change.
      ```typescript
      name: this.fb.control("", {
        validators: [Validators.required, Validators.minLength(5)],
        asyncValidators: [this.beerNameUsedValidator.validate],
        updateOn: "blur", // validate after user leaves the input field
      });
      ```
2. Don't forget to handle the case when you update a beer (you should still be able to use the initial name).
    - Hint: If needed, you can add validators dynamically:
      ```typescript
      this.formGroup.controls.name.addAsyncValidators(
        this.beerNameUsedValidator.validate(this.initialBeer.name) // pass parameter to ignore the initial name
      );
      ```

## Task 15 - Search

1. Add an input to the beers page that can be used to filter beers by name.
    - Hint: You can use the following definition of the input field:
   ```angular2html
   <input type="text" (keyup)="onSearchTermChange($event.target['value'])">
   ```
2. Use RxJS `debounceTime` pipe operator to prevent filtering items too often.
    - 💡 RxJS `debounceTime` - https://www.learnrxjs.io/learn-rxjs/operators/filtering/debouncetime
3. Use RxJS `distinctUntilChanged` pipe operator to ignore the new term if it's the same as the previous one.
    - 💡 RxJS `distinctUntilChanged` - https://www.learnrxjs.io/learn-rxjs/operators/filtering/distinctuntilchanged

## Task 16 - Lazy-loaded modules

1. Move separate pages (home page, beer list, forms) to their own feature modules and make them lazy loaded.
    - 💡 Lazy-loading feature modules - https://angular.io/guide/lazy-loading-ngmodules
2. Compare the size of generated bundle before and after adding lazy loading.
    - Use "Network" tab in browser dev tools.
