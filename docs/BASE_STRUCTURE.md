# Structure

1. Start project
2. Create own component with route
3. Create own pipe
   1. Transform text
4. Create own directive
   1. Change color based on x
5. Display data from static resource
6. Display data from API (use predefined service by changing dependency injection)
   1. http://localhost:3000/api-docs
7. Change service to load data only for your user
8. Create base form (template driven) that will save it to API and reload
9. Create form (reactive) that will save it to API and reload
10. Delete data from api
11. Download detail, update in form and update, display updated version
