#Zdroje informac� o Angularu

##Angular dokumentace 
https://angular.io/docs
- do pole "search" zadejte n�zev komponenty, directivy, service, pipe, class, interface, ... 

##Angular blogy - Zdroj informac� o novink�ch v Angularu
- https://blog.angular.io/
- https://toddmotto.com/
- https://blog.nrwl.io/@vsavkin

##State management - Redux, NgRx
Obecn� popis Redux architektury
- https://redux.js.org/introduction
Jak managovat stav v angular aplikaci
- https://blog.nrwl.io/managing-state-in-angular-applications-22b75ef5625f
- https://www.youtube.com/watch?v=vX2vG0o-rpM
NgRx, Implementace Redux architektury v Angularu
- https://github.com/ngrx/platform/blob/master/docs/store/README.md
NgRx effects
- https://github.com/ngrx/platform/blob/master/docs/effects/README.md

##Forms - Reactive, Template driven
Basic forms (�vod do reactive a template driven form�)
- https://www.youtube.com/watch?v=xYv9lsrV0s4
Advanced forms (custom component, nested forms, ...)
- https://www.youtube.com/watch?v=CD_t3m2WMM8

##Related informace k v�voji Angular aplikace
- BEM - metodika jak stylovat aplikaci (http://getbem.com/)