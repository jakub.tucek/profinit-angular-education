// Modules throwing error "Jest encountered an unexpected token"
const esModules = ["rxjs"].join('|');
module.exports = {
  preset: "jest-preset-angular",
  globalSetup: 'jest-preset-angular/global-setup',
  transformIgnorePatterns: [
    `<rootDir>/node_modules/(?!.*\\.mjs$|${esModules})`,
  ],
};
