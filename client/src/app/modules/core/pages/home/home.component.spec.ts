import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {HomeComponent} from "./home.component";
import {BeersService} from "../../types/beers-service.interface";
import {of} from "rxjs";


class TestBeersMockService implements BeersService {
  createBeer = () => of(null);
  deleteBeer = () => of(null);
  getBeerById = () => of(null);
  getBeers = () => [];
  getBeersAsync = () => of([]);
  searchBeers = () => of(null);
  updateBeer = () => of(null);
}

describe("HomeComponent", () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        HomeComponent
      ],
      providers: [
        {provide: "BeersService", useClass: TestBeersMockService}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  })

  it("should render title", () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector("h1").textContent).toContain("Welcome");
  });
});
