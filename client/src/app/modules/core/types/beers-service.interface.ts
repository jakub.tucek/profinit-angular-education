import {Observable} from "rxjs";

export interface BeersService {

  getBeers(): any[];

  getBeersAsync(): Observable<any[]>;

}
