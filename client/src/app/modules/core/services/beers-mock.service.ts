import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {BeersService} from "../types/beers-service.interface";


@Injectable()
export class BeersMockService implements BeersService {


  beers: any[] = [
    {
      _id: "1",
      name: "Chroust Hoptopie",
      degreeOfBeer: 12,
      originCity: "Prague",
      type: "NEIpa",
      createdAt: new Date("2020-01-02T10:02:13.825Z"),
      createdByUsername: "unknown"
    },
    {
      _id: "2",
      name: "Crazy Clown Whore of Babylon",
      degreeOfBeer: 15,
      originCity: "Prague",
      type: "ALE",
      createdAt: new Date("2020-01-02T10:02:13.825Z"),
      createdByUsername: "unknown"
    },
    {
      _id: "3",
      name: "Plzen",
      degreeOfBeer: 12,
      originCity: "Plzen",
      type: "ALE",
      createdAt: new Date("2020-01-02T10:02:13.825Z"),
      createdByUsername: "unknown"
    }
  ];

  getBeers(): any[] {
    return this.beers;
  }

  getBeersAsync(): Observable<any> {
    return of(this.beers);
  }

}
