import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CoreLayoutComponent} from "./components/layout/layout.component";
import {HomeComponent} from "./pages/home/home.component";


const routes: Routes = [
  {
    path: "",
    component: CoreLayoutComponent,
    children: [
      {
        path: "", component: HomeComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
