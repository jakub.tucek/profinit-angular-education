import {NgModule} from "@angular/core";
import {HomeComponent} from "./pages/home/home.component";
import {CoreRoutingModule} from "./core-routing.module";
import {CoreLayoutComponent} from "./components/layout/layout.component";
import {CoreFooterComponent} from "./components/footer/footer.component";
import {CoreHeaderComponent} from "./components/header/header.component";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [
    HomeComponent,
    CoreLayoutComponent,
    CoreFooterComponent,
    CoreHeaderComponent
  ],
  imports: [
    CoreRoutingModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [HomeComponent]
})
export class CoreModule { }
