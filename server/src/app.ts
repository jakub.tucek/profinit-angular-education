import express, {NextFunction, Request, Response} from "express";
import compression from "compression"; // compresses requests
import session from "express-session";
import bodyParser from "body-parser";
import MongoStore from "connect-mongo";
import flash from "express-flash";
import path from "path";
import mongoose from "mongoose";
import bluebird from "bluebird";

import {MONGODB_URI, SESSION_SECRET} from "./util/secrets";
// Controllers (route handlers)
// API keys and Passport configuration
import {CurdApiController} from "./controllers/crud-api";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const expressOasGenerator = require("express-oas-generator");


// Create Express server
const app = express();
expressOasGenerator.init(app,
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    require("../swagger-ui.json"),
    "../swagger-ui.json",
    undefined,
    undefined,
    undefined,
    undefined,
    []
);

// Connect to MongoDB
const mongoUrl = MONGODB_URI;
mongoose.Promise = bluebird;

mongoose.connect(mongoUrl).then(
    () => {
        console.log("MongoDB connected");
    },
).catch(err => {
    console.log("MongoDB connection error. Please make sure MongoDB is running. " + err);
    process.exit(1);
});


// Express configuration
app.set("port", process.env.PORT || 3000);
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Cache-Control", "no-cache");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");

    next();
});
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: SESSION_SECRET,
    store: MongoStore.create({
        mongoUrl: mongoUrl,
    })
}));
app.use(flash());
app.use(
    express.static(path.join(__dirname, "public"), {maxAge: 31557600000})
);


function handleError() {
    // empty
}

/**
 * Primary app routes.
 */
app.get("/", (...args) => CurdApiController.getSampleData(...args).catch(handleError));
app.get("/beer/", (...args) => CurdApiController.getSampleData(...args).catch(handleError));
app.get("/beer/:username", (...args) => CurdApiController.getSampleData(...args).catch(handleError));
app.put("/beer/:username/:id", (...args) => CurdApiController.updateBeer(...args).catch(handleError));
app.delete("/beer/:username/:id", (...args) => CurdApiController.deleteBeer(...args).catch(handleError));
app.post("/beer/:username", (...args) => CurdApiController.createBeer(...args).catch(handleError));
app.use((req, res, next) => {
    if (!req.url.startsWith("/api-docs") && !res.headersSent && req.method !== "OPTIONS") {
        res.status(404).send({
            code: 404,
            error: "Page not found"
        });
    } else {
        next();
    }
});

app.use((err: Error, req: Request, res: Response, _next: NextFunction) => {
    console.error(`Err: ${err.stack}`);

    if (!res.headersSent) {
        res.status(500).send({
            code: 500,
            error: `Internal error: ${err.message}`
        });
    }
});


export default app;
