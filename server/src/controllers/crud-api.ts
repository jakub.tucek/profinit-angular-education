import {NextFunction, Request, Response} from "express";
import {Beer, BeerDocument, BeerStructure} from "../models/Beer";
import logger from "../util/logger";
import {ObjectId} from "mongodb";


async function randomlyFailOrMakeItSlow(next: NextFunction) {
    await new Promise(resolve => {
        setTimeout(resolve, Math.floor(Math.random() * 1_000));
    });
    if (Math.random() < 0.2) {
        const error = new Error("Maybe you are missing some error handling?");
        next(error);
        await Promise.reject(error);
    }
}

export class CurdApiController {
    static async getSampleData(request: Request, response: Response, next: NextFunction) {
        await randomlyFailOrMakeItSlow(next);
        let query = {};
        if (request.params.username) {
            query = {createdByUsername: request.params.username};
        }
        Beer.find(query, (err, beers: BeerDocument[]) => {
            if (err) next(err);
            response.send({result: beers});
            next();
        });
    }


    static async createBeer(request: Request, response: Response, next: NextFunction) {
        await randomlyFailOrMakeItSlow(next);
        const newBeer = new Beer({
            createdAt: new Date(),
            createdByUsername: request.params.username,
            degreeOfBeer: request.body.degreeOfBeer,
            name: request.body.name,
            originCity: request.body.originCity,
            type: request.body.type
        } as BeerStructure);

        await newBeer.save((err, saved) => {
            if (err) {
                logger.error(err);
                response.statusMessage = err.message;
                return response.sendStatus(403);
            }
            response.status(201).send(saved);
            next();
        });
    }

    static async deleteBeer(request: Request, response: Response, next: NextFunction) {
        await randomlyFailOrMakeItSlow(next);
        CurdApiController.findOneBeer(request, response, next, (beer: BeerDocument) => {
            console.log(beer._id);
            Beer.deleteOne({_id: new ObjectId(beer._id)}, (err => {
                if (err) {
                    response.sendStatus(500);
                }
                console.log("Item deleted");
                response.sendStatus(204);
                next();
            }));
        });
    }

    static async updateBeer(request: Request, response: Response, next: NextFunction) {
        await randomlyFailOrMakeItSlow(next);
        await CurdApiController.findOneBeer(request, response, next, (beer: BeerDocument) => {
            beer.name = request.body.name;
            beer.degreeOfBeer = request.body.degreeOfBeer;
            beer.originCity = request.body.originCity;
            beer.type = request.body.type;

            beer.save((err, saved) => {
                if (err) {
                    logger.error(err);
                    response.statusMessage = err.message;
                    return response.sendStatus(403);
                }
                response.status(204).send(saved);
                next();
            });
        });
    }


    private static async findOneBeer(request: Request,
                                     response: Response,
                                     next: NextFunction,
                                     beerHandler: (beer: BeerDocument) => void) {
        await randomlyFailOrMakeItSlow(next);
        const username = request.params.username;
        const id = request.params.id;

        Beer.find({createdByUsername: username, _id: id}, async (err, beers: BeerDocument[]) => {
            if (err) {
                logger.error(err);
                return response.sendStatus(500);
            }
            if (beers.length === 0) {
                return response.sendStatus(404);
            }
            return beerHandler(beers[0]);
        });
    }
}
