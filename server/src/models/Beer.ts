import * as mongoose from "mongoose";


export interface BeerStructure {
    name: string;
    degreeOfBeer: number;
    originCity: string;
    type: string;
    createdAt: Date;
    createdByUsername: string;
}

export type BeerDocument = mongoose.Document & BeerStructure

const beerSchema = new mongoose.Schema({
    name: String,
    degreeOfBeer: Number,
    originCity: String,
    type: String,
    createdAt: Date,
    createdByUsername: String
});

export const Beer = mongoose.model<BeerDocument>("Beer", beerSchema);
